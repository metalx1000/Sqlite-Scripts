#!/bin/bash

dir="db"
db="$dir/db.sqlite"
table="table1"
pid="$(date +%s)"

echo "Creating Database"
echo "Directory the Database is in must be writeable for web user."
mkdir db

echo "Creating DB..."
sqlite3 $db "create table $table (id INTEGER PRIMARY KEY,pid TEXT);"
echo "Creating pid..."
sqlite3 $db "insert into $table (pid) values ('$pid');"

echo "Entering Fields..."
cat columns.lst|while read line
do
  sqlite3 $db "ALTER TABLE $table ADD $line TEXT;"
  sqlite3 $db "UPDATE $table SET $line = 'test' WHERE pid  = '$pid';"
done

sqlite3 $db "select * from $table";

sudo chown -R www-data:www-data  "$dir"
sudo chmod -R u+w "$dir"

./form.sh
