#!/bin/bash
output="form.php"

echo '<form action="update_db.php" method="GET">' > "$output"
echo '<div class="form-group">' >> "$output"

echo "<label for='pid'>pid:</label>" >> "$output"
echo "<input type='text' class='form-control' id='pid' name='pid' value='$(date +%s)'>" >> "$output"

cat columns.lst|while read l
do
  echo "<label for='$l'>$l:</label>" >> "$output"
  echo "<input type='text' class='form-control' id='$l' name="$l">" >> "$output"
done
echo '</div>' >> "$output"
echo '<button type="submit" class="btn btn-primary btn-block">Submit</button>' >> "$output"
echo '</form>' >> "$output"
